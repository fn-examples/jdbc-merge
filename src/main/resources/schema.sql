CREATE TABLE COUNTRY (
    id   			INTEGER      			NOT NULL AUTO_INCREMENT,
    country_id 		INTEGER 				NOT NULL,
    last_updated 	VARCHAR2(100 CHAR) 		NOT NULL,
    
    PRIMARY KEY (id),
    UNIQUE(country_id)
);

INSERT INTO COUNTRY(country_id, last_updated) VALUES(1, '12:01:00.100');
INSERT INTO COUNTRY(country_id, last_updated) VALUES(2, '12:02:00.200');
INSERT INTO COUNTRY(country_id, last_updated) VALUES(3, '12:03:00.300');