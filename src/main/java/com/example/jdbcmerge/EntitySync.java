package com.example.jdbcmerge;

import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class EntitySync {
	
	private List<?> defaultIgnorableExceptions = Collections.emptyList();
	
	/**
	 * Sync entity by inserting and updating records manually.
	 */
	public void sync() {
		sync(this.defaultIgnorableExceptions);
	}

	/**
	 * Sync entity by inserting and updating records manually.
	 * 
	 * @param ignorableExceptions
	 *            List of ignorable exceptions
	 */
	public void sync(final List<?> ignorableExceptions) {
		try {
			insertRecords();
			updateRecords();
		} catch (Exception e) {
			if (!ignorableExceptions.contains(e.getClass())) {
				log.warn("Error occured while syncing entity!", e);
			}
		}
	}

	/**
	 * Executes logic of inserting records.
	 */
	protected abstract void insertRecords();
	/**
	 * Executes logic of updating records.
	 */
	protected abstract void updateRecords();
}
