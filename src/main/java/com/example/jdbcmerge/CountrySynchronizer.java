package com.example.jdbcmerge;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Component
public class CountrySynchronizer {
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public void sync(final List<Integer> countryIds) {
		List<Integer> countriesToUpdate = determineExistingCountries(countryIds);
		List<Integer> countriesToInsert = new ArrayList<>(countryIds);
		countriesToInsert.removeAll(countriesToUpdate);
		new CountrySync(countriesToInsert, countriesToUpdate).sync();
	}
	
	private List<Integer> determineExistingCountries(final List<Integer> countryIds) {
		final MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("ids", countryIds);

		final String query = "SELECT country_id FROM COUNTRY WHERE country_id IN (:ids)";

		return namedParameterJdbcTemplate.query(query, parameters, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(final ResultSet rs, final int rowNum) throws SQLException {
				return rs.getInt("country_id");
			}
		});
	}
	
	@AllArgsConstructor
	private class CountrySync extends QuietEntitySync {
		private List<Integer> idsToInsert;
		private List<Integer> idsToUpdate;
		
		@Override
		protected void insertRecords() {
			final StringBuilder insertQuery = new StringBuilder();
			insertQuery.append("INSERT INTO COUNTRY(country_id, last_updated) ");
			insertQuery.append("VALUES(:country_id, :last_updated);");
			
			final MapSqlParameterSource[] parameters = new MapSqlParameterSource[idsToInsert.size()];
			IntStream.range(0, idsToInsert.size()).forEach(index -> {
				parameters[index] = new MapSqlParameterSource();
				parameters[index].addValue("country_id", idsToInsert.get(index));
				parameters[index].addValue("last_updated", LocalTime.now());
			});
			
			namedParameterJdbcTemplate.batchUpdate(insertQuery.toString(), parameters);
		}

		@Override
		protected void updateRecords() {
			final StringBuilder updateQuery = new StringBuilder();
			updateQuery.append("UPDATE COUNTRY SET ");
			updateQuery.append("country_id = :country_id, last_updated = :last_updated ");
			updateQuery.append("WHERE country_id = :country_id;");
			
			final MapSqlParameterSource[] parameters = new MapSqlParameterSource[idsToUpdate.size()];
			IntStream.range(0, idsToUpdate.size()).forEach(index -> {
				parameters[index] = new MapSqlParameterSource();
				parameters[index].addValue("country_id", idsToUpdate.get(index));
				parameters[index].addValue("last_updated", LocalTime.now());
			});
			
			namedParameterJdbcTemplate.batchUpdate(updateQuery.toString(), parameters);
			
		}
	}
	
}
