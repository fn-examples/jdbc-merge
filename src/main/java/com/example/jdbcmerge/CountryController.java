package com.example.jdbcmerge;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {
	
	@Autowired
	private CountrySynchronizer countrySynchronizer;

	@GetMapping("/sync")
	public ResponseEntity<Object> syncParties() {
		countrySynchronizer.sync(Arrays.asList(3, 4));
		return ResponseEntity.ok().body("Countries synced");
	}
}
