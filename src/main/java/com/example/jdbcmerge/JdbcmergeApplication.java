package com.example.jdbcmerge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcmergeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdbcmergeApplication.class, args);
	}
}
