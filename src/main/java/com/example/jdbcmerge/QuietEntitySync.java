package com.example.jdbcmerge;

import java.util.Arrays;

import org.springframework.dao.DuplicateKeyException;

public abstract class QuietEntitySync extends EntitySync {
	
	@Override
	public void sync() {
		super.sync(Arrays.asList(DuplicateKeyException.class));
	}

}
